# Tugas Story PPW GASAL 2020/2021

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

Link heroku: http://lastppw2020.herokuapp.com/

- Repositori ini berisi template tugas story PPW tahun ajaran Gasal 2020/2021

- Nama: Mochammad Naufal Rizki

- NPM: 1906299010
- Kelas: PPW-E

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/laymonage/django-template-heroku/-/commits/master
