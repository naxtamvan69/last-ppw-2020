from django.contrib import admin
from .models import listHistory, History
# Register your models here.
admin.site.register(listHistory)
admin.site.register(History)