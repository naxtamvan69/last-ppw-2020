from django import forms
from .models import listHistory,History

class history(forms.ModelForm):
    class Meta:
        model = History
        fields = {
            'name',
        }

        labels = {
            'name': 'Nama Accordion',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'style': 'background-color: #E4E4E4;',
                                             'placeholder': 'ex: Pacar Saya...', }),
        }

class listhistory(forms.ModelForm):
    class Meta:
        model = listHistory
        fields = {
            'name',
            'history',
        }

        labels = {
            'name': 'List Dropdown',
            'history' : 'Accordion'
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'style': 'background-color: #E4E4E4;',
                                           'placeholder': 'ex: Pacar Saya...', }),
            'history' : forms.Select(attrs={'class' : 'form-control', 'style': 'background-color: #E4E4E4;',})
        }