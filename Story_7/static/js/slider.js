$(document).ready(function() {

	$(".btn-slider").click(function(){
    	var self = $(this),
			item = self.parents('div.contentslide'),
    		content = item.children('div.content');
		content.toggleClass('active');
	});

	$('.btn-down').click(function () {
	    var self = $(this),
	        item = self.parents('div.contentslide'),
	        swapWith = item.next();
	    item.before(swapWith);
	});

	$('.btn-up').click(function () {
	    var self = $(this),
	        item = self.parents('div.contentslide'),
	        swapWith = item.prev();
	    item.after(swapWith);
	});
});