from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.test import TestCase, Client
from django.urls import resolve

from .models import listHistory, History
from .views import displayHistory, formHistory, formListHistory
from .forms import history, listhistory

# Create your tests here.

class Test(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', password='test', email='test@example.com')
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_model_History_and_listHistory(self):
        History.objects.create(name= 'Test123')
        count1 = History.objects.all().count()
        self.assertEqual(count1, 1)
        test1 = History.objects.get(name="Test123")
        self.assertEqual(str(test1), "Test123")
        listHistory.objects.create(name='test', history=test1)
        count2 = listHistory.objects.all().count()
        self.assertEqual(count2, 1)
        test2 = listHistory.objects.get(name="test")
        self.assertEqual(str(test2), "test")

    def test_url_dipslayHistory(self):
        response = Client().get('/history/')
        self.assertEqual(response.status_code, 200)

    def test_template_displayHistory(self):
        response = Client().get('/history/')
        self.assertTemplateUsed(response, 'Story_7/display_history.html')

    def test_index_func_displayHistory(self):
        found = resolve('/history/')
        self.assertEqual(found.func, displayHistory)

    '''def test_url_create_accordion(self):
        self.client.force_login(user=self.user)
        response = Client().get('/history/create-accordion')
        self.assertEqual(response.status_code, 200)

    def test_template_create_accordion(self):
        response = Client().get('/history/create-accordion')
        self.assertTemplateUsed(response, 'Story_7/create_accordion.html')'''

    def test_index_func_create_Accrodion(self):
        found = resolve('/history/create-accordion')
        self.assertEqual(found.func, formHistory)

    '''def test_url_create_accordion_dropdown(self):
        response = Client().get('/history/create-accordion-dropdown')
        self.assertEqual(response.status_code, 200)

    def test_template_create_accordion_dropdown(self):
        response = Client().get('/history/create-accordion-dropdown')
        self.assertTemplateUsed(response, 'Story_7/create_accordion_dropdown.html')'''

    def test_index_func_create_accrodion_dropdown(self):
        found = resolve('/history/create-accordion-dropdown')
        self.assertEqual(found.func, formListHistory)

    '''def test_form_crate_accordion(self):
        response = self.client.post('/history/create-accordion', data={'name': 'test'})
        self.assertEqual(response.status_code, 200)

    def test_form_crate_accordio_dropdown(self):
        test1 = History.objects.create(name='Test123')
        response = self.client.post('/history/create-accordion-dropdown', data={'name': 'test', 'history': test1})
        self.assertEqual(response.status_code, 200)'''
