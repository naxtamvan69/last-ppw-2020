from django.urls import path

from . import views

app_name = 'history'

urlpatterns = [
    path('', views.displayHistory, name='display-history'),
    path('create-accordion', views.formHistory, name='create-accordion'),
    path('create-accordion-dropdown', views.formListHistory, name='create-accordion-dropdown'),
]