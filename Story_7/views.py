from django.shortcuts import render, redirect
from .models import listHistory, History
from .forms import listhistory, history

# Create your views here.

def displayHistory(request):
    listhistory = listHistory.objects.all()
    history = History.objects.all()
    context = {
        'listhistory' : listhistory,
        'history' : history,
    }
    return render(request, 'Story_7/display_history.html', context)

def formHistory(request):
    form_history = history(request.POST or None)
    context = {
        'form_history' : form_history,
    }
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form_history.is_valid():
                form_history.save()
                return redirect('history:display-history')
        return render(request, 'Story_7/create_accordion.html', context)
    else:
        return redirect('history:display-history')

def formListHistory(request):
    form_listhistory = listhistory(request.POST or None)
    context = {
        'form_listhistory' : form_listhistory,
    }
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form_listhistory.is_valid():
                form_listhistory.save()
                return redirect('history:display-history')
        return render(request, 'Story_7/create_accordion_dropdown.html', context)
    else:
        return redirect('history:display-history')