$(document).ready(function() {

    $("#keyword").keyup(function() {
        var key = $("#keyword").val();
        //console.log(key);
        $.ajax({
            url: '/search/data/?q=' + key,
            success: function(data) {
                //console.log(data.items);
                var accordion = $('.accordion');
                accordion.empty();
                for (i = 0; i < data.items.length; i++) {
                    var title = data.items[i].volumeInfo.title;
                    var author = data.items[i].volumeInfo.authors;
                    var pageCount = data.items[i].volumeInfo.pageCount;
                    var categories = data.items[i].volumeInfo.categories;
                    var description = data.items[i].volumeInfo.description;
                    var image = data.items[i].volumeInfo.imageLinks.smallThumbnail;
                    //console.log(title);
                    accordion.append(
                        '<div class="contentslide">' +
                            '<div class="label row">' +
                                '<div class="col-md-11 col-10">' +
                                     '<p>' + title + '</p>' +
                                '</div>' +
                                '<div class="col-md-1 col-2">' +
                                    '<button type="button" class="btn btn-primary btn-slider">+</button>' +
                                '</div>' +
                            '</div>' +
                            '<div class="content">' +
                                '<img class="img" src='+ image +'>' +
                                '<table style="width: 100%">' +
                                    '<tbody>' +
                                        '<tr>' +
                                            '<th class="th">Judul</th>' +
                                            '<td>' + title + '</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<th class="th">Penulis</th>' +
                                            '<td>' + author + '</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<th class="th">Jumlah Halaman</th>' +
                                            '<td>' + pageCount + ' Halaman' + '</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<th class="th">Kategori</th>' +
                                            '<td>' + categories + '</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<th class="th">Deskripsi</th>' +
                                            '<td>' + description + '</td>' +
                                        '</tr>' +
                                    '</tbody>' +
                                '</table>' +
                            '</div>' +
                        '</div>'
                    )
                }
            }
        });
    });

    $(".accordion").on('click', '.btn-slider', function() {

        var self = $(this),
            item = self.parents('div.contentslide'),
            content = item.children('div.content');
        console.log(self);
        console.log(content);
        content.toggleClass('active');
    });
});