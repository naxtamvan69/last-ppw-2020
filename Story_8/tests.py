from django.test import TestCase, Client
from django.urls import resolve

from .views import searchBooks, data

# Create your tests here.

class Test(TestCase):
    def test_url_search(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_template_search(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'Story_8/search.html')

    def test_index_func_search(self):
        found = resolve('/search/')
        self.assertEqual(found.func, searchBooks)

    def test_url_searchBooksData(self):
        response = Client().get('/search/data/?q=')
        self.assertEqual(response.status_code, 200)

    def test_url_search_is_exist(self):
        response = Client().get('/search/')
        isi = response.content.decode('utf8')
        self.assertIn('Pencarian Buku', isi)

    def test_url_searchBooksData_is_exist(self):
        response = Client().get('/search/data/?q=sword%20art%20online')
        isi = response.content.decode('utf8')
        self.assertIn('Sword Art Online Progressive 6 (light novel)', isi)