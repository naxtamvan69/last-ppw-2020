from django.urls import path

from . import views

app_name = 'search'

urlpatterns = [
    path('', views.searchBooks, name='search-books'),
    path('data/', views.data),
]