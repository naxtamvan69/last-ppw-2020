from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.

def searchBooks(request):
    context = {}
    return render(request, 'Story_8/search.html', context)

def data(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url)
    context = json.loads(r.content)
    return JsonResponse(context, safe=False)