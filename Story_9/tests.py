from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .views import SignIn, SignUp, LogOut

# Create your tests here.

class Test(TestCase):
    def setUp(self):
        self.user = {
            'email' : 'test@example.com',
            'username' : 'test',
            'password' : 'test',
        }

        self.sign = {
            'username' : 'test',
            'password' : 'test'
        }
        #self.account = User.objects.create_user(username='test', email='test@example.com', password='test')
        #self.account.save()
        return super().setUp()

    def test_url_SignIn(self):
        response = Client().get('/user/signin/')
        self.assertEqual(response.status_code, 200)

    def test_template_SignIn(self):
        response = Client().get('/user/signin/')
        self.assertTemplateUsed(response, 'Story_9/signin.html')

    def test_index_func_SignIn(self):
        found = resolve('/user/signin/')
        self.assertEqual(found.func, SignIn)

    def test_url_SignUp(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code, 200)

    def test_template_SignUp(self):
        response = Client().get('/user/signup/')
        self.assertTemplateUsed(response, 'Story_9/signup.html')

    def test_index_func_SignUp(self):
        found = resolve('/user/signup/')
        self.assertEqual(found.func, SignUp)

    def test_url_LogOut(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code, 200)

    def test_template_LogOut(self):
        response = Client().get('/user/logout/')
        self.assertTemplateUsed(response, 'Story_9/logout.html')

    def test_index_func_LogOut(self):
        found = resolve('/user/logout/')
        self.assertEqual(found.func, LogOut)

    def test_SignUp(self):
        response_post = Client().post('/user/signup/', self.user, format='text/html')
        self.assertEqual(response_post.status_code, 302)

    def test_SignIn(self):
        response_post = Client().post('/user/signin/', self.sign, format='text/html')
        self.assertEqual(response_post.status_code, 200)
        test = User.objects.create_user(username='test', email='test', password='test')
        self.client.force_login(test)


    def test_LogOut(self):
        response = self.client.get('/user/logout/')
        self.assertEqual(response.status_code, 200)