from django.urls import path

from .views import SignUp, SignIn, LogOut

app_name = 'user'

urlpatterns = [
    path('signup/', SignUp, name='signup'),
    path('logout/', LogOut, name='logout'),
    path('signin/', SignIn, name='signin'),
]