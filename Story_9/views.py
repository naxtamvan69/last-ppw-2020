from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import messages

# Create your views here.
def SignUp(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        user = User.objects.create_user(username, email, password)
        user.save()
        login(request, user)
        return redirect('main:home')

    return render(request, 'Story_9/signup.html')

def LogOut(request):
    if request.method == 'POST':
        boolean = request.POST['boolean']
        if boolean == "true":
            logout(request)
        return redirect('main:home')
    return render(request, 'Story_9/logout.html')

def SignIn(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('main:home')
        else:
            messages.info(request, "Nama atau password yang anda masukkan salah")
    return render(request, 'Story_9/signin.html')

